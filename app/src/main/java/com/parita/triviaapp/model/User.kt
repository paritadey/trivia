package com.parita.triviaapp

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "trivia_game_table")
data class User(
    @ColumnInfo(name = "date")
    val date: String,
    @ColumnInfo(name = "time")
    var time: String,
    @PrimaryKey
    @ColumnInfo(name = "username")
    var username: String,
    @ColumnInfo(name = "cricketer")
    var cricketer: String,
    @ColumnInfo(name = "indianFlag")
    var indianFlag: String,
)
