package com.parita.triviaapp.repository

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.parita.triviaapp.History
import com.parita.triviaapp.TriviaGameDatabase
import com.parita.triviaapp.User
import com.parita.triviaapp.UserDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class TriviaGameRepository(private val userDao: UserDao) {
    companion object {
        var triviaGameDatabase: TriviaGameDatabase? = null
        fun initializeDB(context: Context): TriviaGameDatabase {
            return TriviaGameDatabase.getInstance(context)
        }

        fun insertTask(context: Context, user: User) {
            triviaGameDatabase = initializeDB(context)
            CoroutineScope(IO).launch {
                triviaGameDatabase!!.triviaGameDatabaseDao().insert(user)
            }
        }

    }
}