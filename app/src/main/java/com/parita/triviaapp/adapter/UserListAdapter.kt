package com.parita.triviaapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.parita.triviaapp.R
import com.parita.triviaapp.User

class UserListAdapter(val userList: List<User>) :
    RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: UserListAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: User) {
            val dateTime: TextView = itemView.findViewById(R.id.date_time)
            val name: TextView = itemView.findViewById(R.id.name)
            val answerTwo: TextView = itemView.findViewById(R.id.answer_two)
            val answerThree: TextView = itemView.findViewById(R.id.answer_three)
            dateTime.text = user.date + "\t" + user.time
            name.text = "User Name: " + user.username
            answerTwo.text = "Answer 2: " + user.cricketer
            answerThree.text = "Answer 3:" + user.indianFlag
        }
    }
}

