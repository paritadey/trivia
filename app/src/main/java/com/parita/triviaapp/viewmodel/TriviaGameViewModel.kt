package com.parita.triviaapp.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.parita.triviaapp.TriviaGameDatabase
import com.parita.triviaapp.User
import com.parita.triviaapp.repository.TriviaGameRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TriviaGameViewModel : ViewModel() {

    protected val compositeDisposable = CompositeDisposable()

    private var dataBaseInstance: TriviaGameDatabase? = null

    var personsList = MutableLiveData<List<User>>()

    fun setInstanceOfDb(dataBaseInstance: TriviaGameDatabase) {
        this.dataBaseInstance = dataBaseInstance
    }

    fun insertData(context: Context, user: User) {
        TriviaGameRepository.insertTask(context, user)
    }


    fun getUsersHistory() {
        dataBaseInstance?.triviaGameDatabaseDao()?.getHistoryDetails()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                if (!it.isNullOrEmpty()) {
                    personsList.postValue(it)
                } else {
                    personsList.postValue(listOf())
                }
                it?.forEach {
                    Log.d("Person Name", it.username)
                }
            }, {
            })?.let {
                compositeDisposable.add(it)
            }

    }

    override fun onCleared() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
        super.onCleared()
    }

}
