package com.parita.triviaapp

import java.io.Serializable

interface Communicator {
    fun passData(edittext_input: String)
    fun passDataBundle(name:String, cricker:String)

}