package com.parita.triviaapp

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = arrayOf(User::class), version = 1, exportSchema = false)
abstract class TriviaGameDatabase : RoomDatabase() {

    abstract fun triviaGameDatabaseDao(): UserDao
    companion object {

        @Volatile
        private var INSTANCE: TriviaGameDatabase? = null

        fun getInstance(context: Context): TriviaGameDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        TriviaGameDatabase::class.java,
                        "trivia_history_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }

}




