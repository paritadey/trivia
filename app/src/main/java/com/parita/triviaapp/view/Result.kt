package com.parita.triviaapp

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.parita.triviaapp.viewmodel.TriviaGameViewModel
import java.io.Serializable
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class Result : AppCompatActivity() {
    var TAG: String? = Result::class.simpleName
    lateinit var answerOne: TextView
    lateinit var answerTwo: TextView
    lateinit var answerThree: TextView
    lateinit var finish: Button
    lateinit var history: Button
    lateinit var triviaGameViewModel: TriviaGameViewModel
    lateinit var context: Context
    var triviaGameDatabase: TriviaGameDatabase? = null
    val list: ArrayList<String> = ArrayList()


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        context = this@Result
        triviaGameViewModel = ViewModelProvider(this).get(TriviaGameViewModel::class.java)
        triviaGameDatabase = TriviaGameDatabase.getInstance(this)
        val currentDate: String = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        val currentTime: String = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())
        var username: String? = intent.getStringExtra("name")
        var cricketer: String? = intent.getStringExtra("cricketer")
        val list: ArrayList<String>? = intent.getStringArrayListExtra("flags")
        Log.d(
            TAG,
            "User name: " + username + "\t" + cricketer + "\t" + currentDate + "\t" + currentTime
        )
        answerOne = findViewById(R.id.answer_one)
        answerTwo = findViewById(R.id.answer_two)
        answerThree = findViewById(R.id.answer_three)

        answerOne.text = "Answer 1: " + username
        answerTwo.text = "Answer 2: " + cricketer
        val stringOfNames = list?.joinToString(", ")
        answerThree.text = "Answer 3: " + stringOfNames
        finish = findViewById(R.id.finish)
        finish.setOnClickListener {
            val userObj = User(
                currentDate,
                currentTime,
                username.toString(),
                cricketer.toString(),
                stringOfNames.toString()
            )
            triviaGameViewModel.insertData(context, userObj)
            Toast.makeText(baseContext, "Trivia Game Data is inserted", Toast.LENGTH_LONG).show()
            val intent = Intent(this@Result, MainActivity::class.java);
            startActivity(intent);
            finish()
        }
        history = findViewById(R.id.history)
        history.setOnClickListener {
            val userObj = User(
                currentDate,
                currentTime,
                username.toString(),
                cricketer.toString(),
                stringOfNames.toString()
            )
            triviaGameViewModel.insertData(context, userObj)
            Toast.makeText(baseContext, "Trivia Game Data is inserted", Toast.LENGTH_LONG).show()
            val intent = Intent(context, History::class.java)
            startActivity(intent)
            finish()
        }
    }
}



