package com.parita.triviaapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity(), Communicator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var name = findViewById<EditText>(R.id.edittext_input)
        var next = findViewById<Button>(R.id.next)
        next.setOnClickListener {
            if (!name.text.toString().trim().isEmpty()) {
                val text = name.text
                passData(text.toString())
            } else {
                name.error = "Please enter the username"
            }
        }
    }

    override fun passData(edittext_input: String) {
        val intent = Intent(this@MainActivity, Cricketer::class.java);
        intent.putExtra("name", edittext_input)
        startActivity(intent);
        finish()
    }

    override fun passDataBundle(name: String, cricker: String) {
        TODO("Not yet implemented")
    }

}