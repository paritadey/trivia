package com.parita.triviaapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast

class Flag : AppCompatActivity() {
    var TAG: String? = Flag::class.simpleName
    lateinit var white: CheckBox
    lateinit var yellow: CheckBox
    lateinit var orange: CheckBox
    lateinit var green: CheckBox
    lateinit var next: Button
    val list: ArrayList<String> = ArrayList()
    lateinit var context: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flag)
        context = this@Flag
        var username: String? = intent.getStringExtra("name")
        var cricketer: String? = intent.getStringExtra("cricketer")
        Log.d(TAG, "User name: " + username + "\t" + cricketer)
        white = findViewById(R.id.white_check)
        yellow = findViewById(R.id.yellow_check)
        orange = findViewById(R.id.orange_check)
        green = findViewById(R.id.green_check)
        next = findViewById(R.id.next)
        next.setOnClickListener {
            if (white.isChecked)
                list.add("White")
            if (yellow.isChecked)
                list.add("Yellow")
            if (orange.isChecked)
                list.add("Orange")
            if (green.isChecked)
                list.add("Green")
            passData(context, list, username, cricketer)
        }
    }

    private fun passData(
        context: Context,
        list: ArrayList<String>,
        username: String?,
        cricketer: String?
    ) {
        if (list.size > 0) {
            val intent = Intent(this@Flag, Result::class.java);
            intent.putExtra("name", username)
            intent.putExtra("cricketer", cricketer)
            intent.putStringArrayListExtra("flags", list)
            startActivity(intent);
            finish()
        } else {
            Toast.makeText(context, "Please choose option", Toast.LENGTH_LONG).show()
        }
    }
}