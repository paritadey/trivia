package com.parita.triviaapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast

class Cricketer : AppCompatActivity(), Communicator {
    var TAG: String? = Cricketer::class.simpleName
    var radioGroup: RadioGroup? = null
    lateinit var radioButton: RadioButton
    private lateinit var next: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cricketer)
        var username: String? = intent.getStringExtra("name")
        Log.d(TAG, "User name: " + username)

        radioGroup = findViewById(R.id.cricketer_group)
        next = findViewById(R.id.next)
        next.setOnClickListener {
            val intSelectButton: Int = radioGroup!!.checkedRadioButtonId
            if (intSelectButton != -1) {
                radioButton = findViewById(intSelectButton)
                passDataBundle(username.toString(), radioButton.text.toString())
                //Toast.makeText(baseContext, radioButton.text, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(baseContext, "Please choose an option", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun passData(edittext_input: String) {
        TODO("Not yet implemented")
    }

    override fun passDataBundle(name: String, cricker: String) {
        val intent = Intent(this@Cricketer, Flag::class.java);
        intent.putExtra("name", name)
        intent.putExtra("cricketer", cricker)
        startActivity(intent);
        finish()
    }
}