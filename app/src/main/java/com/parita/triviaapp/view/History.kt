package com.parita.triviaapp

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.parita.triviaapp.adapter.UserListAdapter
import com.parita.triviaapp.viewmodel.TriviaGameViewModel

class History : AppCompatActivity() {
    lateinit var context: Context
    var triviaGameDatabase: TriviaGameDatabase? = null
    var recyclerView: RecyclerView? = null
    private var triviaGameViewModel: TriviaGameViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        context = this@History
        triviaGameViewModel = ViewModelProviders.of(this).get(TriviaGameViewModel::class.java)

        triviaGameDatabase = TriviaGameDatabase.getInstance(this)
        triviaGameViewModel?.setInstanceOfDb(triviaGameDatabase!!)

        initViews()
        observerViewModel()
    }

    private fun observerViewModel() {
        triviaGameViewModel?.personsList?.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                handleData(it)
            } else {
                handleZeroCase()
            }
        })
    }

    private fun initViews() {
        recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        triviaGameViewModel?.getUsersHistory()
    }

    private fun handleData(data: List<User>) {
        recyclerView?.visibility = View.VISIBLE
        var adapter = UserListAdapter(data)
        recyclerView?.adapter = adapter
    }

    private fun handleZeroCase() {
        recyclerView?.visibility = View.GONE
        Toast.makeText(this, "No Records Found", Toast.LENGTH_LONG).show()
    }

}